/*
Name : app.cpp
Author: Amit Malyala
Description:
Version: 0.1
Notes:
coding Log:
22-07-20. Created initial version. Project compiles with G++ in ISO C++14
23-07-20 Added operators =,+,-,/,* and others into NewInt class . Added range checking and exceptions.
08-05-20 Changed operators to return objects by value instead of references. Changed definitions of most operators.
         Added data hiding and encapsulation features.

To do:

Known Issues:
None
Unknown issues:
None

To do:

*/
#include "intlib.h"
#include <iostream>

/* Execute application */
void TestExecApplication(void);

SINT32 main(void) 
{
	TestExecApplication();
	return 0;
}

/* Execute application */
void TestExecApplication(void)
{
	try
	{
		NewInt z = 2;
		NewInt x = 2 +z;
		std::cout << z <<std::endl;
		std::cout << z +2<<std::endl;
		std::cout << x <<std::endl;
		// z should be 2 here.
		std::cout << "z should be 2" <<std::endl;
		std::cout << z <<std::endl;
		z = x++;
		// z should be 4
		std::cout << "z should be 4" <<std::endl;
		std::cout << z <<std::endl;
		// x should be 5
		std::cout << "x should be 5" <<std::endl;
		std::cout << x <<std::endl;
		x++;
		// x should be 6
		std::cout << "x should be 6" <<std::endl;
		std::cout << x <<std::endl;
		std::cout << "z should be -6" <<std::endl;
		z = -x;
		std::cout << z <<std::endl;
		std::cout << "z should be 7" <<std::endl;
		z = 1 + -z;
		std::cout << z <<std::endl;
		std::cout << "z should be 5" <<std::endl;
		z = -x - 1;
		std::cout << z <<std::endl;
		std::cout << "z should be -5" <<std::endl;
		z = -x + 1;
		std::cout << z <<std::endl;
		
		std::cout << "z should be 6" <<std::endl;
		z = 1 + -z;
		std::cout << z <<std::endl;
		
		std::cout << "x should be -6" << std::endl;
		std::cout << x <<std::endl;
		
		std::cout << "z should be +66" <<std::endl;
		z = 60 + -x ;
		std::cout << z <<std::endl;
		std::cout << "x should be 6" << std::endl;
		x=6;
		std::cout << x <<std::endl;
		std::cout << "z should be -54" <<std::endl;
		z = x-60 ;
		std::cout << z <<std::endl;
		x=5;
		z = 80/x +x ;
		std::cout << "z should be 21" <<std::endl;
		std::cout << z <<std::endl;
		
		std::cout << ~z << std::endl;
		NewInt b;
		b= 147483647;
	    std::cout <<b <<std::endl;
	    b++;
	    std::cout <<++b << std::endl;
	    ++b;
	    std::cout <<b<< std::endl;
	    NewInt c = (7+8)-(6);
	    c =b+c;
	    std::cout <<c << std::endl;
		b+=c;
	    NewInt arr[10];
	    for (int i=0;i<10;i++)
	    {
	       arr[i]=i+1;
		}
	    for (int i=0;i<10;i++)
	    {
	    	std::cout << arr[i] << std::endl;
		}
		NewInt a =82147483647; 
	}
	catch (std::exception const& e) 
	{
        std::cout << e.what() << std::endl;
    }
}
