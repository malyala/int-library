/*
Name : intlib.h
Author: Amit Malyala , copyright Amit Malyala 2020, All rights reserved.
Description:
This is a integer library which creates a data type as int  which functions 
in all arithmetic operators. Instead of overflowing or underflowing, this new type 
would throw exceptions.
Version: 0.01
Notes:
File being edited
*/
#include <limits>
#include <stdexcept>
#include "std_types.h"
#include <ostream>


#define INTTYPE 1
#define FLOATTYPE 2
#define DOUBLETYPE 3

/*
Class: NewInt 
Description:
This class uses a variant type to define a int like data type.
Almost any builtin type can be defined with this class. 
This class would throw a exception when this type exceeds intmax 
as std::numeric_limits<int>::max() or goes below intmin as std::numeric_limits<int>::min.
instead of overflowing. A longlong variable is used to hold the value of this new Int inside a variant.

Notes: Class being edited.
*/
class NewInt
{
	private:
	Variant var;
	public:
    
	NewInt();
	~NewInt();
	//Copy assignment for a = 1;
		//For Newint a; 
	NewInt (const NewInt& rhs);
	// For NewInt a = 2; type initialization
	NewInt (LONGLONG rhs);
	NewInt& operator= (LONGLONG arg);
	// For a = b; type copy assignment
    NewInt& operator = (const NewInt& rhs);
    /* operator complement ~ */
    NewInt operator~ ();
    NewInt operator! ();
	NewInt operator++ ();
	NewInt operator++ (int);
	NewInt operator-- ();
	NewInt operator-- (int);
	NewInt operator* (LONGLONG arg);
	NewInt operator* ( const NewInt& rhs);
	friend NewInt operator*(LONGLONG var, NewInt& b);
	NewInt operator+ (LONGLONG arg);
	NewInt operator+ ( const NewInt& rhs);
	NewInt& operator+ (void);
	friend NewInt operator+(LONGLONG var, NewInt& b);
	NewInt operator- (LONGLONG arg);
	NewInt& operator-(void );
	NewInt operator- ( const NewInt& rhs);
	friend NewInt operator- (LONGLONG lhs, NewInt& b);
	NewInt operator/ (LONGLONG arg);
	NewInt operator/ ( const NewInt& rhs);	
	friend NewInt operator/(LONGLONG lhs, NewInt& b);
	NewInt operator% (LONGLONG arg);
	NewInt operator% ( const NewInt& rhs);
	friend NewInt operator%(LONGLONG lhs, NewInt& b);
	NewInt& operator*= (LONGLONG arg);
	NewInt& operator*= ( const NewInt& rhs);
	NewInt& operator+= (LONGLONG arg);
	NewInt& operator+= ( const NewInt& rhs);
	NewInt& operator-= (LONGLONG arg);
	NewInt& operator-= ( const NewInt& rhs);
	NewInt& operator/= (LONGLONG arg);
	NewInt& operator/= ( const NewInt& rhs);
	NewInt& operator%= (LONGLONG arg);
	NewInt operator<< (LONGLONG arg);
	NewInt& operator<< ( const NewInt& rhs);
	friend NewInt operator<< (LONGLONG lhs, NewInt& b);
	NewInt operator>> (LONGLONG arg);
	NewInt& operator>> ( const NewInt& rhs);
	friend NewInt operator>> (LONGLONG lhs, NewInt& b);
	NewInt& operator>>= (LONGLONG arg);
	BOOL  operator== (const NewInt& rhs);
	BOOL  operator== (LONGLONG rhs);
	BOOL  operator!= (const NewInt& rhs);
	BOOL  operator!= (LONGLONG rhs);
    /* Get value */
    inline LONGLONG GetValue(void) const;
    /* Set value */
	inline void SetValue(LONGLONG v);
	/* Check range to throw exception */
	inline void CheckRange(Variant var);
	/* Display output with << operator using var object */
	friend std::ostream& operator<<(std::ostream& os,  NewInt obj);
};
