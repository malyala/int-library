/*************************************************************************************
Name : intlib.cpp
Author: Amit Malyala , copyright Amit Malyala 2020, All rights reserved.
Description:
This is a integer library which creates a data type as int  which functions 
with all arithmetic operators. Instead of overflowing or underflowing, this new type 
would throw exceptions.
Version: 0.06
Notes:
More operators to be added.
****************************************************************************************/
#include "intlib.h"

/*
Constructor for NewInt class
*/
NewInt::NewInt()
{
	var.Type= INTTYPE;
	var.isDataInitialized=true;
	var.u.longlongValue=0;
}
/*
Destructor for NewInt class
*/
NewInt::~NewInt()
{
	
}
/* Get value */
inline LONGLONG NewInt::GetValue(void) const
{
   return var.u.longlongValue;
}

/* Function to check range */
inline void NewInt::CheckRange(Variant var)
{
	/*
	int max: 2147483647
	int min: -2147483648
	*/
	if (var.u.longlongValue < std::numeric_limits<int>::min())
	{
		throw std::underflow_error("Error: underflow detected");
	}
	else if (var.u.longlongValue > std::numeric_limits<int>::max())
	{
		throw std::overflow_error("Error: overflow detected ");
	}
}

/* Set value */
inline void NewInt::SetValue(LONGLONG v)
{
	var.u.longlongValue =v;
}

// For NewInt a = 2; type initialization. Copy constructor 
NewInt::NewInt (LONGLONG rhs)
{
	this->SetValue(rhs);
	CheckRange(var);
}
//For Newint a; copy constructor 
NewInt::NewInt (const NewInt& rhs)
{
	this->SetValue(rhs.GetValue());
	CheckRange(var);
}
//Copy assignment for a = 1;
NewInt& NewInt::operator= (LONGLONG arg) 
{
	this->SetValue(arg);
	CheckRange(var);
	return *this;
}

// For a = b; type copy assignment
NewInt& NewInt::operator = (const NewInt& rhs) 
{
    this->SetValue( rhs.GetValue());
   	CheckRange(var);
   	return *this;
}

/* operator prefix ! */
NewInt NewInt::operator! ()
{
	NewInt Temp;
	Temp.SetValue(!this->GetValue());
	CheckRange(Temp.var);
	return Temp;
}

/* operator prefix ~ */
NewInt NewInt::operator~ ()
{
	NewInt Temp;
	Temp.SetValue(~this->GetValue() );
	CheckRange(Temp.var);
	return Temp;
}

/* operator prefix ++ */
NewInt NewInt::operator++ ()
{
	NewInt Temp;
	Temp.SetValue(this->GetValue()+1 );
	this->SetValue(this->GetValue()+1);
	CheckRange(Temp.var);
	return Temp;
}

/* operator postfix ++ */
NewInt NewInt::operator++ (int)
{
	NewInt Temp;
	this->SetValue (this->GetValue()+1);
	CheckRange(Temp.var);
	return Temp;
}
/* operator prefix -- */
NewInt NewInt::operator-- ()
{
	NewInt Temp;
	Temp.SetValue(this->GetValue()-1 );
	this->SetValue(this->GetValue()-1 );
	CheckRange(Temp.var);
	return Temp;
}
/* operator postfix -- */
NewInt NewInt::operator-- (int)
{
	NewInt Temp = *this;
	this->SetValue(this->GetValue()-1 );
	CheckRange(Temp.var);
	return Temp;
}

NewInt NewInt::operator* (LONGLONG arg)
{
	NewInt Temp;
	Temp.SetValue( this->GetValue() *arg);
	CheckRange(Temp.var);
	return Temp;
}
NewInt NewInt::operator* ( const NewInt& rhs)
{
	NewInt Temp =*this;
	Temp.SetValue(this->GetValue() * rhs.GetValue() );
	CheckRange(Temp.var);
	return Temp;
}

NewInt operator*(LONGLONG lhs, NewInt& b)
{
	NewInt Temp;
	Temp.SetValue(lhs * b.GetValue());
	return Temp;
}


NewInt NewInt::operator+ (LONGLONG arg)
{
	NewInt Temp;
	Temp.SetValue( this->GetValue() +arg);
	CheckRange(Temp.var);
	return Temp;
}
NewInt NewInt::operator+ ( const NewInt& rhs)
{
	NewInt Temp;
	Temp.SetValue(this->GetValue()+ rhs.GetValue());
	CheckRange(Temp.var);
	return Temp;
}

NewInt operator+(LONGLONG var, NewInt& b)
{
	NewInt Temp;
	Temp.SetValue(b.GetValue() +var);
	return Temp;
}
NewInt& NewInt::operator+ (void)
{
	return *this;
}
NewInt NewInt::operator- (LONGLONG arg)
{
	NewInt Temp;
	Temp.SetValue( this->GetValue() -arg);
	CheckRange(Temp.var);
	return Temp;
}
NewInt NewInt::operator- ( const NewInt& rhs)
{
	this->SetValue(this->GetValue()-rhs.GetValue());
	CheckRange(var);
	return *this;
}
// unary minus
NewInt& NewInt::operator- (void)
{
	this->SetValue( -this->GetValue() );
	CheckRange(var);
	return *this;
}

NewInt operator-(LONGLONG lhs, NewInt& b)
{
	NewInt Temp;
	Temp.SetValue(lhs - b.GetValue());
	return Temp;
}

NewInt NewInt::operator/ (LONGLONG arg)
{
	NewInt Temp;
	Temp.SetValue( this->GetValue() /arg);
	CheckRange(Temp.var);
	return Temp;
}
NewInt NewInt::operator/ ( const NewInt& rhs)
{
	NewInt Temp = *this;
	Temp.SetValue(this->GetValue() / rhs.GetValue());
	CheckRange(Temp.var);
	return Temp;
}

NewInt operator/(LONGLONG lhs, NewInt& b)
{
	NewInt Temp;
	Temp.SetValue(lhs/ b.GetValue());
	return Temp;
}
NewInt NewInt::operator% (LONGLONG arg)
{
	NewInt Temp;
	Temp.SetValue( this->GetValue() %arg);
	CheckRange(Temp.var);
	return Temp;
}				
NewInt NewInt::operator% ( const NewInt& rhs)
{
	NewInt Temp = *this;
	Temp.SetValue(this->GetValue() % rhs.GetValue());
	CheckRange(Temp.var);
	return Temp;
}
NewInt operator% (LONGLONG lhs, NewInt& b)
{
	NewInt Temp;
	Temp.SetValue(lhs % b.GetValue());
	return Temp;
}
		
NewInt& NewInt::operator*= (LONGLONG arg)
{
	this->SetValue(this->GetValue()*arg );
	CheckRange(var);
	return *this;
}	
NewInt& NewInt::operator*= ( const NewInt& rhs)
{
	this->SetValue(this->GetValue()* rhs.GetValue());
	CheckRange(var);
	return *this;
}	
NewInt& NewInt::operator+= (LONGLONG arg)
{
	this->SetValue(this->GetValue()+arg );
	CheckRange(var);
	return *this;
}	
	
NewInt& NewInt::operator+= ( const NewInt& rhs)
{
	this->SetValue(this->GetValue()+rhs.GetValue() );
	CheckRange(var);
	return *this;
}	
NewInt& NewInt::operator-= (LONGLONG arg)
{
	this->SetValue (this->GetValue()-arg );
	CheckRange(var);
	return *this;
}	
	
NewInt& NewInt::operator-= ( const NewInt& rhs)
{
	this->SetValue (this->GetValue()- rhs.GetValue() );
	CheckRange(var);
	return *this;
}
NewInt& NewInt::operator/= (LONGLONG arg)
{
	this->SetValue(this->GetValue()/arg );
	CheckRange(var);
	return *this;
}		
NewInt& NewInt::operator/= ( const NewInt& rhs)
{
	this->SetValue(this->GetValue()/  rhs.GetValue() );
	CheckRange(var);
	return *this;
}	
	
NewInt& NewInt::operator%= (LONGLONG arg)
{
	this->SetValue(this->GetValue() %arg );
	CheckRange(var);
	return *this;
}	
NewInt NewInt::operator<< (LONGLONG arg)
{
	NewInt Temp;
	Temp.SetValue( this->GetValue()  << arg);
	CheckRange(Temp.var);
	return Temp;
}

NewInt& NewInt::operator<< ( const NewInt& rhs)
{
	this->SetValue (this->GetValue() << rhs.GetValue());
	CheckRange(var);
	return *this;
}
NewInt operator<<(LONGLONG lhs, NewInt& b)
{
	NewInt Temp;
	Temp.SetValue(lhs << b.GetValue());
	return Temp;
}
NewInt NewInt::operator>> (LONGLONG arg)
{
	NewInt Temp;
	Temp.SetValue(this->GetValue() >>arg );
	CheckRange(Temp.var);
	return Temp;
}
NewInt& NewInt::operator>> ( const NewInt& rhs)
{
    this->SetValue(this->GetValue()>> rhs.GetValue() );
	CheckRange(var);
	return *this;
}
NewInt operator>> (LONGLONG lhs, NewInt& b)
{
    NewInt Temp;
	Temp.SetValue(lhs >> b.GetValue());
	return Temp;
}
NewInt& NewInt::operator>>= (LONGLONG arg)
{
	this->SetValue(this->GetValue()>>arg );
	CheckRange(var);
	return *this;
}
BOOL NewInt::operator== (const NewInt& rhs)
{
	return  (this->GetValue() == rhs.GetValue());
}

BOOL NewInt::operator== (LONGLONG rhs)
{
	return  (this->GetValue() == rhs);
}

BOOL  NewInt::operator!= (const NewInt& rhs)
{
	return  (this->GetValue() != rhs.GetValue());
}
BOOL NewInt::operator!= (LONGLONG rhs)
{
	return  (this->GetValue() != rhs);
}

std::ostream& operator<<(std::ostream& os,  NewInt obj)
{
	os << obj.GetValue();
    return os;
}

